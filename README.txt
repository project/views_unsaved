List any unsaved views when a user visits the admin/stucture/views page.

Sometimes you accidentally navigate away from a view you're working on without
clicking save. Rather than clicking the browser Back button or remembering the
machine name, you can now simply go the main Views list page and click the
description or name of the view you were editing to get back to it.

Thanks to Colin Foley for the idea.
